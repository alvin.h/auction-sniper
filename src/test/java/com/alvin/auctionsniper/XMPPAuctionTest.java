package com.alvin.auctionsniper;

import com.alvin.auctionsniper.e2e.FakeAuctionServer;
import com.alvin.auctionsniper.xmpp.XMPPAuction;
import org.jivesoftware.smack.XMPPConnection;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CountDownLatch;

public class XMPPAuctionTest {
    private final XMPPConnection connection = new XMPPConnection(FakeAuctionServer.XMPP_HOSTNAME);
    private final FakeAuctionServer auctionServer = new FakeAuctionServer("item-12345");

    @Test
    public void receivesEventsFromAuctionServerAfterJoining() {
        CountDownLatch auctionWasClosed = new CountDownLatch(1);

        Auction auction = new XMPPAuction(connection, auctionServer.getItemId());
        auction.addAuctionEventListener(auctionClosedListener(auctionWasClosed));
    }

    private AuctionEventListener auctionClosedListener(final CountDownLatch auctionWasClosed) {
        return new AuctionEventListener() {
            @Override
            public void auctionClosed() {
                auctionWasClosed.countDown();
            }

            @Override
            public void auctionFailed() {

            }

            @Override
            public void currentPrice(int price, int increment, PriceSource from) {

            }
        };
    }
}
