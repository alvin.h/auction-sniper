package com.alvin.auctionsniper.e2e;

import com.alvin.auctionsniper.Main;
import com.alvin.auctionsniper.SniperState;
import com.alvin.auctionsniper.xmpp.XMPPAuction;
import com.alvin.auctionsniper.ui.MainWindow;

import static com.alvin.auctionsniper.ui.SnipersTableModel.textFor;

public class ApplicationRunner {
    public static final String SNIPER_ID = "sniper3";
    public static final String SNIPER_PASSWORD = "sniper";
    public static final String SNIPER_XMPP_ID = "sniper3@antop.org/" + XMPPAuction.AUCTION_RESOURCE;
    private AuctionSniperDriver driver;

    private void runBiddingThread(Thread thread) {
        thread.setDaemon(true);
        thread.start();

        driver = new AuctionSniperDriver(1000);
        driver.hasTitle(MainWindow.APPLICATION_TITLE);
        driver.hasColumnTitles();
    }

    public void startBiddingIn(final FakeAuctionServer... auctions) {
        Thread thread = new Thread("Test Application") {
            @Override
            public void run() {
                try {
                    Main m = new Main(FakeAuctionServer.XMPP_HOSTNAME, SNIPER_ID, SNIPER_PASSWORD);
                    for(FakeAuctionServer auction : auctions) {
                        m.onClickJoinAuction(auction.getItemId(), Integer.MAX_VALUE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        runBiddingThread(thread);
    }

    public void startBiddingWithStopPrice(final FakeAuctionServer auction, int stopPrice) {
        Thread thread = new Thread("Test Application") {
            @Override
            public void run() {
                try {
                    Main m = new Main(FakeAuctionServer.XMPP_HOSTNAME, SNIPER_ID, SNIPER_PASSWORD);
                    m.onClickJoinAuction(auction.getItemId(), stopPrice);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        runBiddingThread(thread);
    }

    public void hasShownSniperIsBidding(FakeAuctionServer auction, int lastPrice, int lastBid) {
        driver.showsSniperStatus(auction.getItemId(), lastPrice, lastBid, textFor(SniperState.BIDDING));
    }

    public void hasShownSniperIsWinning(FakeAuctionServer auction, int winningBid) {
        driver.showsSniperStatus(auction.getItemId(), winningBid, winningBid, textFor(SniperState.WINNING));
    }

    public void hasShownSniperIsLosing(FakeAuctionServer auction, int lastPrice, int lastBid) {
        driver.showsSniperStatus(auction.getItemId(), lastPrice, lastBid, textFor(SniperState.LOSING));
    }

    public void showsSniperHasWonAuction(FakeAuctionServer auction, int lastPrice) {
        driver.showsSniperStatus(auction.getItemId(), lastPrice, lastPrice, textFor(SniperState.WON));
    }

    public void showsSniperHasLostAuction(FakeAuctionServer auction, int lastPrice, int lastBid) {
        driver.showsSniperStatus(auction.getItemId(), lastPrice, lastBid, textFor(SniperState.LOST));
    }

    public void showsSniperHasFailed(FakeAuctionServer auction) {

    }

    public void reportsInvalidMessage(FakeAuctionServer auction, String brokenMessage) {

    }

    public void stop() {
        if (driver != null) {
            driver.dispose();
        }
    }
}
