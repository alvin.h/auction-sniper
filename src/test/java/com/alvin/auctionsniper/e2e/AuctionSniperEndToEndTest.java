package com.alvin.auctionsniper.e2e;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

public class AuctionSniperEndToEndTest {
    static {
        System.setProperty("com.objogate.wl.keyboard", "GB");
    }

    public static final String ITEM_ID = "item-12345";
    public static final String ITEM_ID_2 = "item-23456";
    private final FakeAuctionServer auction = new FakeAuctionServer(ITEM_ID);
    private final FakeAuctionServer auction2 = new FakeAuctionServer(ITEM_ID_2);
    private final ApplicationRunner application = new ApplicationRunner();

    @Test
    public void sniperJoinsAuctionUntilAuctionCloses() throws Exception {
        // 1단계: [서버] 품목 판매 시작
        auction.startSellingItem();
        // 2단계: [어플] 입찰 시작
        application.startBiddingIn(auction);
        // 3단계: [서버] 스나이퍼로부터 가입 요청을 받았는지 확인
        auction.hasReceivedJoinRequestFromSniper();
        // 4단계: [서버] 경매 폐쇠 알림
        auction.announceClosed();
        // 5단계: [어플] 경매가 낙찰 되었는지 확인
        application.showsSniperHasLostAuction(auction, 0, 0);
    }

    @Test
    public void sniperMakesAHigherBidButLoses() throws Exception {
        // 1단계: [서버] 품목 판매 시작
        auction.startSellingItem();

        // 2단계: [어플] 입찰 시작
        application.startBiddingIn(auction);
        // 3단계: [서버] 스나이퍼로부터 가입 요청을 받았는지 확인
        auction.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);

        // 4단계: [서버]
        auction.reportPrice(1000, 98, "other bidder");
        // 5단계: [어플] 스나이퍼가 경매로부터 가격 갱신 메시지를 받고 난 후 입찰하고 있는지 확인해달라고 서버에 요청
        application.hasShownSniperIsBidding(auction, 1000, 1098);

        // 6단계: [서버] 스나이퍼로부터 입찰을 받았고 해당 입찰이 마지막 가격에 최소 증가액을 더한 것과 가격이 같은지 확인
        auction.hasReceivedBid(1098, ApplicationRunner.SNIPER_XMPP_ID);

        // 7단계: [서버] 여전이 스나이퍼가 경매에서 낙찰을 못했으므로 경매를 종료함
        auction.announceClosed();
        application.showsSniperHasLostAuction(auction, 1000, 1098);
    }

    @Test
    public void sniperWinsAnAuctionByBiddingHigher() throws Exception {
        // 1단계: [서버] 품목 판매 시작
        auction.startSellingItem();

        // 2단계: [어플] 입찰 시작
        application.startBiddingIn(auction);
        // 3단계: [서버] 스나이퍼로부터 가입 요청을 받았는지 확인
        auction.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);

        // 4단계: [서버]
        auction.reportPrice(1000, 98, "other bidder");
        // 5단계: [어플] 스나이퍼가 경매로부터 가격 갱신 메시지를 받고 난 후 입찰하고 있는지 확인해달라고 서버에 요청
        application.hasShownSniperIsBidding(auction, 1000, 1098);

        // 6단계: [서버] 스나이퍼로부터 입찰을 받았고 해당 입찰이 마지막 가격에 최소 증가액을 더한 것과 가격이 같은지 확인
        auction.hasReceivedBid(1098, ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(1098,97, ApplicationRunner.SNIPER_XMPP_ID);
        application.hasShownSniperIsWinning(auction, 1098);

        // 7단계: [서버] 여전이 스나이퍼가 경매에서 낙찰을 못했으므로 경매를 종료함
        auction.announceClosed();
        application.showsSniperHasWonAuction(auction, 1098);
    }

    @Test
    public void sniperWindsAnAuctionByBiddingHigher() throws Exception {
        auction.startSellingItem();

        application.startBiddingIn(auction);
        auction.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(1000, 98, "other bidder");
        application.hasShownSniperIsBidding(auction, 1000, 1098);

        auction.hasReceivedBid(1098, ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(1098, 97, ApplicationRunner.SNIPER_XMPP_ID);
        application.hasShownSniperIsWinning(auction, 1098);

        auction.announceClosed();
        application.showsSniperHasWonAuction(auction, 1098);
    }

    @Test
    public void sniperBidsForMultipleItems() throws Exception {
        auction.startSellingItem();
        auction2.startSellingItem();

        application.startBiddingIn(auction, auction2);
        auction.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);
        auction2.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(1000, 98, "other bidder");
        auction.hasReceivedBid(1098, ApplicationRunner.SNIPER_XMPP_ID);

        auction2.reportPrice(500, 21, "other bidder");
        auction2.hasReceivedBid(521, ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(1098, 97, ApplicationRunner.SNIPER_XMPP_ID);
        auction2.reportPrice(521, 22, ApplicationRunner.SNIPER_XMPP_ID);

        application.hasShownSniperIsWinning(auction, 1098);
        application.hasShownSniperIsWinning(auction2, 521);

        auction.announceClosed();
        auction2.announceClosed();
    }

    @Test
    public void snipersLosesAnAuctionWhenThePriceIsTooHigh() throws Exception {
        auction.startSellingItem();
        application.startBiddingWithStopPrice(auction, 1100);
        auction.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(1000, 98, "other bidder");
        application.hasShownSniperIsBidding(auction, 1000, 1098);

        auction.hasReceivedBid(1098, ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(1197, 10, "third party");
        application.hasShownSniperIsLosing(auction, 1197, 1098);

        auction.reportPrice(1207, 10, "fourth party");
        application.hasShownSniperIsLosing(auction, 1207, 1098);

        auction.announceClosed();
        application.showsSniperHasLostAuction(auction, 1207, 1098);
    }

    @Test
    public void sniperReportsInvalidAuctionMessageAndStopsRespondingToEvents() throws Exception {
        String brokenMessage = "a broken message";
        auction.startSellingItem();
        auction2.startSellingItem();

        application.startBiddingIn(auction, auction2);
        auction.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);

        auction.reportPrice(500, 20, "other bidder");
        auction.hasReceivedBid(520, ApplicationRunner.SNIPER_XMPP_ID);

        auction.sendInvalidMessageContaining(brokenMessage);
        application.showsSniperHasFailed(auction);

        auction.reportPrice(520, 21, "other bidder");
        waitForAnotherAuctionEvent();

        application.reportsInvalidMessage(auction, brokenMessage);
        application.showsSniperHasFailed(auction);
    }

    private void waitForAnotherAuctionEvent() throws Exception {
        auction2.hasReceivedJoinRequestFrom(ApplicationRunner.SNIPER_XMPP_ID);
        auction2.reportPrice(600, 6, "other bidder");
        application.hasShownSniperIsBidding(auction2, 600, 606);
    }

    @AfterEach
    public void stopAuction() {
        auction.stop();
        auction2.stop();
    }

    @AfterEach
    public void stopApplication() {
        application.stop();
    }
}
