package com.alvin.auctionsniper.unit;

import com.alvin.auctionsniper.SniperSnapshot;
import com.alvin.auctionsniper.ui.Column;
import com.alvin.auctionsniper.ui.SnipersTableModel;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SnipersTableModelTest {
    private final TableModelListener listener = mock(TableModelListener.class);
    private final SnipersTableModel model = new SnipersTableModel();

    @BeforeEach
    void attachModelListener() {
        model.addTableModelListener(listener);
    }

    @Test
    void hasEnoughColumns() {
        assertThat(model.getColumnCount(), CoreMatchers.equalTo(Column.values().length));
    }

    @Test
    void setsSniperValuesInColumns() {
        SniperSnapshot joining = SniperSnapshot.joining("item id");
        SniperSnapshot bidding = joining.bidding(555, 666);

        model.addSniperSnapshot(joining);
//        verify(listener, times(1)).tableChanged(refEq(new TableModelEvent(model)));

        model.sniperStateChanged(bidding);
        assertRowMatchesSnapshot(0, 1, 555);
    }

    @Test
    void notifiesListenersWhenAddingASniper() {
        SniperSnapshot joining = SniperSnapshot.joining("item123");

        assertEquals(0, model.getRowCount());

        model.addSniperSnapshot(joining);
//        verify(listener).tableChanged(refEq(new TableModelEvent(model, 0)));

        assertEquals(1, model.getRowCount());
        assertRowMatchesSnapshot(0, 0, "item123");
    }

    @Test
    void holdsSnipersInAdditionOrder() {
        model.addSniperSnapshot(SniperSnapshot.joining("item 0"));
        model.addSniperSnapshot(SniperSnapshot.joining("item 1"));

        assertRowMatchesSnapshot(0, 0, "item 0");
        assertRowMatchesSnapshot(1, 0, "item 1");
    }

    private void assertRowMatchesSnapshot(int rowIndex, int columnIndex, Object expected) {
        assertEquals(expected, model.getValueAt(rowIndex, columnIndex));
    }
}
