package com.alvin.auctionsniper.unit;

import com.alvin.auctionsniper.*;
import com.alvin.auctionsniper.SniperListener;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.alvin.auctionsniper.AuctionEventListener.PriceSource;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuctionSniperTest {
    private final String ITEM_ID = "test-item";
    private final Auction auction = Mockito.mock(Auction.class);
    private final SniperListener sniperListener = Mockito.mock(SniperListener.class);
    private final SniperListener sniperListener2 = Mockito.mock(SniperListener.class);
    private final AuctionSniper sniper = new AuctionSniper(new Item(ITEM_ID, Integer.MAX_VALUE), auction);
    private final AuctionSniper sniper2 = new AuctionSniper(new Item(ITEM_ID, 1100), auction);
    private final ArgumentCaptor<SniperSnapshot> argument = ArgumentCaptor.forClass(SniperSnapshot.class);

    public AuctionSniperTest() {
        this.sniper.addSniperListener(sniperListener);
        this.sniper2.addSniperListener(sniperListener2);
    }

    @Test
    void reportsLostWhenAuctionClosesImmediately() {
        sniper.auctionClosed();
        verify(sniperListener).sniperStateChanged(argument.capture());
        assertEquals(SniperState.LOST, argument.getValue().state);
    }

    @Test
    void bidsHigherAndReportsBiddingWhenNewPriceArrives() {
        final int price = 1001;
        final int increment = 25;
        final int bid = price + increment;
        // action #1
        sniper.currentPrice(price, increment, PriceSource.FromOtherBidder);

        verify(auction).bid(price + increment);
        verify(sniperListener).sniperStateChanged(refEq(new SniperSnapshot(ITEM_ID, price, bid, SniperState.BIDDING)));
    }

    @Test
    void reportsIsWinningWhenCurrentPriceComesFromSniper() {
        sniper.currentPrice(123, 45, PriceSource.FromOtherBidder);
        sniper.currentPrice(135, 45, PriceSource.FromSniper);

        verify(sniperListener, times(2)).sniperStateChanged(argument.capture());
        assertEquals(SniperState.WINNING, argument.getValue().state);
    }

    @Test
    void reportsLostIfAuctionClosesWhenBidding() {
        // 다른 입찰자가 입찰
        sniper.currentPrice(123, 45, PriceSource.FromOtherBidder);
        verify(sniperListener).sniperStateChanged(refEq(new SniperSnapshot(ITEM_ID, 123, 123 + 45, SniperState.BIDDING)));
        // 경매 종료
        sniper.auctionClosed();
        // verify
        verify(sniperListener, atLeastOnce()).sniperStateChanged(argument.capture());
        assertEquals(SniperState.LOST, argument.getValue().state);
    }

    @Test
    void reportsWonIfAuctionClosesWhenWinning() {
        // 스나이퍼가 입찰
        sniper.currentPrice(123, 45, PriceSource.FromSniper);
        // verify: [낙찰중] 상태 확인
        verify(sniperListener).sniperStateChanged(argument.capture());
        assertEquals(SniperState.WINNING, argument.getValue().state);
        // 경매 종료
        sniper.auctionClosed();
        // 낙찰
        verify(sniperListener, atLeastOnce()).sniperStateChanged(argument.capture());
        assertEquals(SniperState.WON, argument.getValue().state);
    }

    @Test
    public void reportsLosesAnAuctionWhenThePriceIsTooHigh() {
        sniper2.currentPrice(1000, 98, PriceSource.FromOtherBidder);
        verify(sniperListener2, times(1)).sniperStateChanged(argument.capture());
        assertEquals(SniperState.BIDDING, argument.getValue().state);

        sniper2.currentPrice(1197, 10, PriceSource.FromOtherBidder);
        verify(sniperListener2, times(2)).sniperStateChanged(argument.capture());
        assertEquals(SniperState.LOSING, argument.getValue().state);

        sniper2.currentPrice(1207, 10, PriceSource.FromOtherBidder);
        verify(sniperListener2, times(3)).sniperStateChanged(argument.capture());
        assertEquals(SniperState.LOSING, argument.getValue().state);

        sniper2.auctionClosed();
        verify(sniperListener2, times(4)).sniperStateChanged(argument.capture());
        assertEquals(SniperState.LOST, argument.getValue().state);
    }

    @Test
    public void reportsFailedIfAuctionFailsWhenBiddng() {
        ignoreAuction();
        allowingSniperBidding();

        expectSniperToFailWhenItIs("bidding");

        sniper.currentPrice(123, 45, PriceSource.FromOtherBidder);
        sniper.auctionFailed();
    }

    private void expectSniperToFailWhenItIs(String message) {

    }

    private void allowingSniperBidding() {

    }

    private void ignoreAuction() {


    }
}
