package com.alvin.auctionsniper.xmpp;

import com.alvin.auctionsniper.AuctionEventListener;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;

import java.util.HashMap;
import java.util.Map;

import static com.alvin.auctionsniper.AuctionEventListener.PriceSource;

public class AuctionMessageTranslator implements MessageListener {
    private final String sniperId;
    private final AuctionEventListener listener;

    public AuctionMessageTranslator(String sniperId, AuctionEventListener listener) {
        this.sniperId = sniperId;
        this.listener = listener;
    }

    public void processMessage(Chat chat, Message message) {
        try {
            translate(message.getBody());
        } catch (Exception e) {
            listener.auctionFailed();
        }
    }

    private void translate(String body) throws Exception {
        AuctionEvent event = AuctionEvent.from(body);

        String eventType = event.type();
        if ("CLOSE".equals(eventType)) {
            listener.auctionClosed();
        } else if("PRICE".equals(eventType)) {
            listener.currentPrice(
                    event.currentPrice(),
                    event.increment(),
                    event.isFrom(sniperId));
        }
    }
    private static class AuctionEvent {
        private final Map<String, String> fields = new HashMap<>();
        public String type() throws Exception { return get("Event"); }
        public int currentPrice() throws Exception { return getInt("CurrentPrice"); }
        public int increment() throws Exception { return getInt("Increment"); }

        public PriceSource isFrom(String sniperId) throws Exception {
            return sniperId.equals(bidder()) ? PriceSource.FromSniper : PriceSource.FromOtherBidder;
        }

        private String bidder() throws Exception {
            return get("Bidder");
        }

        private int getInt(String fieldName) throws Exception {
            return Integer.parseInt(get(fieldName));
        }

        private String get(String name) throws Exception {
            String value = fields.get(name);
            if (null == value) {
                throw new Exception(name);
            }
            return value;
        }

        private void addField(String field) {
            String[] pair = field.split(":");
            fields.put(pair[0].trim(), pair[1].trim());
        }

        static AuctionEvent from(String messageBody) {
            AuctionEvent event = new AuctionEvent();

            for (String field : fieldsIn(messageBody)) {
                event.addField(field);
            }

            return event;
        }

        static String[] fieldsIn(String messageBody) {
            return messageBody.split(";");
        }
    }
}
