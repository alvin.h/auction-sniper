package com.alvin.auctionsniper.xmpp;

import com.alvin.auctionsniper.Auction;
import com.alvin.auctionsniper.AuctionHouse;
import com.alvin.auctionsniper.Item;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;

import static com.alvin.auctionsniper.xmpp.XMPPAuction.AUCTION_RESOURCE;

public class XMPPAuctionHouse implements AuctionHouse {
    private XMPPConnection connection;

    private XMPPAuctionHouse(String hostname, String username, String password) throws XMPPException {
        connection = connection(hostname, username, password);
    }

    @Override
    public Auction auctionFor(Item item) {
        return new XMPPAuction(connection, item.identifier);
    }

    @Override
    public void disconnect() {
        connection.disconnect();
    }

    public static XMPPAuctionHouse connect(String hostname, String username, String password) throws XMPPException {
        return new XMPPAuctionHouse(hostname, username, password);
    }

    private static XMPPConnection connection(String hostname, String username, String password) throws XMPPException {
        XMPPConnection connection = new XMPPConnection(hostname);
        connection.connect();
        connection.login(username, password, AUCTION_RESOURCE);
        return connection;
    }
}
