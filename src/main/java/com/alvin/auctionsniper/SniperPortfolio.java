package com.alvin.auctionsniper;

import com.alvin.auctionsniper.ui.SnipersTableModel;
import com.alvin.auctionsniper.ui.SwingThreadSniperListener;

public class SniperPortfolio implements SniperCollector {
    private SnipersTableModel model;

    @Override
    public void addSniper(AuctionSniper sniper) {
        model.sniperAdded(sniper);
        sniper.addSniperListener(new SwingThreadSniperListener(model));
    }

    public void addPortfolioListener(SnipersTableModel model) {
        this.model = model;
    }
}
