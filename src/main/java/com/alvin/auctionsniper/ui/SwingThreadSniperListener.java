package com.alvin.auctionsniper.ui;

import com.alvin.auctionsniper.SniperListener;
import com.alvin.auctionsniper.SniperSnapshot;

import javax.swing.*;

public class SwingThreadSniperListener implements SniperListener {
    private SnipersTableModel mainWindow;

    public SwingThreadSniperListener(SnipersTableModel mainWindow) {
        this.mainWindow = mainWindow;
    }

    @Override
    public void sniperStateChanged(final SniperSnapshot snapshot) {
        SwingUtilities.invokeLater(() -> mainWindow.sniperStateChanged(snapshot));
    }
}
