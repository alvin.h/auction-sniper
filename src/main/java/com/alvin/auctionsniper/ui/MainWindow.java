package com.alvin.auctionsniper.ui;

import com.alvin.auctionsniper.Item;
import com.alvin.auctionsniper.Main;
import com.alvin.auctionsniper.SniperPortfolio;
import com.alvin.auctionsniper.UserRequestListener;
import com.alvin.auctionsniper.util.Announcer;

import javax.swing.*;
import java.awt.*;

public class MainWindow extends JFrame {
    public static final String APPLICATION_TITLE = "Auction Sniper";
    private static final String SNIPERS_TABLE_NAME = "snipers-table";
    private final SniperPortfolio portfolio;
    private final Announcer<UserRequestListener> userRequests = Announcer.to(UserRequestListener.class);

    public MainWindow(SniperPortfolio portfolio) {
        super(APPLICATION_TITLE);

        this.portfolio = portfolio;
        setName(Main.MAIN_WINDOW_NAME);

        fillContentPane(makeSnipersTable());
        pack();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public void addUserRequestListener(UserRequestListener userRequestListener) {
        userRequests.addListener(userRequestListener);
    }

    public void emitClickEvent(String itemId, int stopPrice) {
        userRequests.announce().joinAuction(new Item(itemId, stopPrice));
    }

    private void fillContentPane(JTable snipersTable) {
        final Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(new JScrollPane(snipersTable), BorderLayout.CENTER);
    }

    private JTable makeSnipersTable() {
        SnipersTableModel model = new SnipersTableModel();
        portfolio.addPortfolioListener(model);
        JTable snipersTable = new JTable(model);
        snipersTable.setName(SNIPERS_TABLE_NAME);
        return snipersTable;
    }
}