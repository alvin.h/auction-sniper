package com.alvin.auctionsniper;

public class AuctionSniper implements AuctionEventListener {
    public final Item item;
    public final Auction auction;
    private SniperSnapshot snapshot;
    private SniperListener sniperListener;

    public AuctionSniper(Item item, Auction auction) {
        this.item = item;
        this.auction = auction;
        this.snapshot = SniperSnapshot.joining(item.identifier);
    }

    @Override
    public void auctionClosed() {
        this.snapshot = this.snapshot.closed();
        notifyChange();
    }

    @Override
    public void auctionFailed() {
        this.snapshot = this.snapshot.failed();
    }

    @Override
    public void currentPrice(int price, int increment, PriceSource priceSource) {
        switch(priceSource) {
            case FromSniper:
                this.snapshot = this.snapshot.winning(price);
                break;
            case FromOtherBidder:
                int bid = price + increment;
                if (item.allowsBid(bid)) {
                    auction.bid(bid);
                    this.snapshot = this.snapshot.bidding(price, bid);
                } else {
                    this.snapshot = this.snapshot.losing(price);
                }
                break;
        }

        notifyChange();
    }

    public void notifyChange() {
        this.sniperListener.sniperStateChanged(snapshot);
    }

    public void addSniperListener(SniperListener sniperListener) {
        this.sniperListener = sniperListener;
    }

    public SniperSnapshot getSnapshot() {
        return this.snapshot;
    }
}
