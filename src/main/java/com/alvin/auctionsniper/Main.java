package com.alvin.auctionsniper;

import com.alvin.auctionsniper.ui.*;
import com.alvin.auctionsniper.xmpp.XMPPAuctionHouse;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Main {
    public static final String MAIN_WINDOW_NAME = "auction-sniper-main";

    private final SniperPortfolio portfolio = new SniperPortfolio();
    private MainWindow ui;

    public Main() throws Exception {
        SwingUtilities.invokeAndWait(() -> ui = new MainWindow(portfolio));
    }

    public Main(String... args) throws Exception {
        this();

        XMPPAuctionHouse auctionHouse = XMPPAuctionHouse.connect(args[0], args[1], args[2]);
        disconnectionWhenUICloses(auctionHouse);
        addUserRequestListenerFor(auctionHouse);
    }

    public void onClickJoinAuction(String itemId, int stopPrice) {
        ui.emitClickEvent(itemId, stopPrice);
    }

    private void disconnectionWhenUICloses(AuctionHouse auctionHouse) {
        ui.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                auctionHouse.disconnect();
            }
        });
    }

    private void addUserRequestListenerFor(AuctionHouse auctionHouse) {
        ui.addUserRequestListener((item) -> {
            SniperLauncher launcher = new SniperLauncher(auctionHouse, portfolio);
            launcher.joinAuction(item);
        });
    }

    public static void main(String... args) throws Exception {
        if(args.length == 0) {
            args = new String[] { "antop.org", "sniper3", "sniper" };
        }

        Main main = new Main(args);
        main.onClickJoinAuction("item-12345", 12345);
        main.onClickJoinAuction("item-23456", 12345);
    }
}