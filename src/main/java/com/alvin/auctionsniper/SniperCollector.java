package com.alvin.auctionsniper;

import com.alvin.auctionsniper.AuctionSniper;

public interface SniperCollector {
    void addSniper(AuctionSniper sniper);
}
